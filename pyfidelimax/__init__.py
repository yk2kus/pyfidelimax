from .auth import GetToken, LojaPertencentesPrograma,\
    CadastrarConsumidor, AtualizarConsumidor, ExtratoConsumidor, ConsultaConsumidor,\
    RetornaDadosCliente,ResgataPremio,PontuaConsumidor,ListaProdutos
